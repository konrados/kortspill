package com.example.idatt2003.konrados.kortspill;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * <h1>HandOfCards</h1>
 * The HandOfCards class represents a hand of cards in a card game.
 */
public class HandOfCards {

  private final ArrayList<PlayingCard> hand;

  /**
   * Checks if the number of cards to return is within bounds.
   *
   * @param n the number of cards to return
   * @throws IllegalArgumentException if n is less than 1 or greater than the size of the hand.
   */

  private void numberWitinBounds(int n) throws IllegalArgumentException {
    if (hand.isEmpty()) {
      throw new IllegalArgumentException("Hand is empty. No cards avalible");
    }
    if (n < 0 || n >= hand.size()) {
      throw new IllegalArgumentException(
          "Number of cards to return must be between 0 and " + (hand.size() - 1));
    }
  }


  /**
   * Creates a new hand of cards.
   */

  public HandOfCards() {
    hand = new ArrayList<>();
  }

  /**
   * Adds a card to the hand.
   *
   * @param card the card to add
   */
  public void addCard(PlayingCard card) {
    hand.add(card);
  }

  /**
   * Adds a list of cards to the hand.
   *
   * @param cards the list of cards to add
   */
  public void addCards(Collection<PlayingCard> cards) {
    hand.addAll(cards);
  }

  /**
   * removes a card from the hand.
   *
   * @param card the card to remove
   */
  public void removeCard(PlayingCard card) {
    hand.remove(card);
  }

  /**
   * removes a card from the hand at a given index.
   *
   * @param index the index of the card to remove
   */
  public void removeCard(int index) {
    numberWitinBounds(index);
    hand.remove(index);
  }

  /**
   * clears the hand of all cards.
   */
  public void clearHand() {
    hand.clear();
  }

  /**
   * Returns the number of cards in the hand.
   *
   * @return the number of cards in the hand
   */
  public int getNumberOfCards() {
    return hand.size();
  }

  /**
   * returns the card at a given index.
   *
   * @param index the index of the card to return
   * @return the card at the given index
   */
  public PlayingCard getCard(int index) {
    numberWitinBounds(index);
    return hand.get(index);
  }

  /**
   * Checks if card is in hand.
   *
   * @return true if the hand has the card, false otherwise
   */
  public boolean cardInHand(PlayingCard card) {
    return hand.contains(card);
  }

  /**
   * Checks if the hand of cards has a flush.
   *
   * @return true if the hand has a flush, false otherwise
   */
  public boolean checkForFlush() {
    return hand.stream().allMatch(card -> card.getSuit() == hand.get(0).getSuit());
  }

  /**
   * Returns the sum of the face values of the cards in the hand.
   *
   * @return the sum of the face values of the cards in the hand
   */
  public int getSumOfCards() {
    return hand.stream()
        .mapToInt(PlayingCard::getFace)
        .sum();
  }

  /**
   * Returns the number of cards with a given suit.
   *
   * @param suit the suit to count
   * @return the number of cards with the given suit
   */
  public String cardsWithSuit(char suit) {
    return hand.stream()
        .filter(card -> card.getSuit() == suit)
        .map(PlayingCard::getAsString)
        .collect(Collectors.joining(" "));

  }


}
