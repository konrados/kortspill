package com.example.idatt2003.konrados.kortspill;

import java.util.List;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class CardGameView extends Application {

  private final HandOfCards hand = new HandOfCards();
  private final DeckOfCards deck = new DeckOfCards();
  private List<Label> cardLabels;
  private Label queenValue;
  private Label flushValue;
  private Label heartsValue;
  private Label sumValue;

  private List<ImageView> cardImages;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) {
    primaryStage.setTitle("Card Game");
    StackPane root = GenerateUi();
    primaryStage.setScene(new Scene(root, 800, 600));
    primaryStage.show();
  }

  private StackPane GenerateUi() {

    GridPane topLeftGrid = new GridPane();
    topLeftGrid.setHgap(10);
    topLeftGrid.setVgap(10);
    topLeftGrid.setPadding(new Insets(10, 10, 10, 10));

    // Sum
    Rectangle sumRectangle = new Rectangle(100, 30, Color.LIGHTGRAY);
    Label sumLabel = new Label("Sum: ");
    sumValue = new Label("0");

    sumLabel.setFont(new Font("Arial", 17));
    sumValue.setFont(new Font("Arial", 20));
    sumValue.setTextFill(Color.WHITE);

    topLeftGrid.add(sumLabel, 0, 0);
    topLeftGrid.add(sumRectangle, 0, 1);
    topLeftGrid.add(sumValue, 0, 1);

    // Hearts
    Rectangle heartsRectangle = new Rectangle(150, 30, Color.LIGHTGRAY);
    Label heartsLabel = new Label("Hearts: ");
    heartsValue = new Label("None");

    heartsLabel.setFont(new Font("Arial", 17));
    heartsValue.setFont(new Font("Arial", 20));
    heartsValue.setTextFill(Color.WHITE);

    topLeftGrid.add(heartsLabel, 1, 0);
    topLeftGrid.add(heartsRectangle, 1, 1);
    topLeftGrid.add(heartsValue, 1, 1);

    // Flush
    Rectangle flushRectangle = new Rectangle(100, 30, Color.LIGHTGRAY);
    Label flushLabel = new Label("Flush: ");
    flushValue = new Label("False");

    flushLabel.setFont(new Font("Arial", 17));
    flushValue.setFont(new Font("Arial", 20));
    flushValue.setTextFill(Color.WHITE);

    topLeftGrid.add(flushLabel, 0, 2);
    topLeftGrid.add(flushRectangle, 0, 3);
    topLeftGrid.add(flushValue, 0, 3);

    // Queen of spades
    Rectangle queenRectangle = new Rectangle(150, 30, Color.LIGHTGRAY);
    Label queenLabel = new Label("Queen of spades: ");
    queenValue = new Label("False");

    queenLabel.setFont(new Font("Arial", 17));
    queenValue.setFont(new Font("Arial", 20));
    queenValue.setTextFill(Color.WHITE);

    topLeftGrid.add(queenLabel, 1, 2);
    topLeftGrid.add(queenRectangle, 1, 3);
    topLeftGrid.add(queenValue, 1, 3);

    //Button grid
    GridPane buttonGrid = new GridPane();
    buttonGrid.setHgap(10);
    buttonGrid.setVgap(10);
    buttonGrid.setPadding(new Insets(10, 10, 10, 10));

    //Draw button
    Button drawButton = new Button("Draw");
    drawButton.setPrefSize(125, 60);
    drawButton.setAlignment(Pos.CENTER);
    drawButton.setFont(new Font("Arial", 25));
    drawButton.setTextFill(Color.WHITE);
    drawButton.setStyle("-fx-background-color: #888888");
    buttonGrid.add(drawButton, 0, 0);

    drawButton.setOnAction(e -> drawCard());

    //Check button
    Button checkButton = new Button("Check");
    checkButton.setPrefSize(125, 60);
    checkButton.setAlignment(Pos.CENTER);
    checkButton.setFont(new Font("Arial", 25));
    checkButton.setTextFill(Color.WHITE);
    checkButton.setStyle("-fx-background-color: #888888");
    buttonGrid.add(checkButton, 0, 1);

    checkButton.setOnAction(e -> checkHand());

    //Card view
    GridPane cardGrid = new GridPane();
    cardGrid.setHgap(20);
    cardGrid.setPadding(new Insets(10, 10, 10, 10));
    cardGrid.setTranslateX(100);
    cardGrid.setTranslateY(400);

    Rectangle cardBackground = new Rectangle(600, 200, Color.LIGHTGRAY);
    cardBackground.setTranslateX(0);
    cardBackground.setTranslateY(200);

    cardImages = List.of(
        new ImageView(),
        new ImageView(),
        new ImageView(),
        new ImageView(),
        new ImageView()
    );

    for (int i = 0; i < cardImages.size(); i++) {
      ImageView card = cardImages.get(i);
      card.setFitWidth(100);
      card.setFitHeight(150);
      cardGrid.add(card, i, 0);
    }

    StackPane root = new StackPane();
    topLeftGrid.setTranslateX(50);
    topLeftGrid.setTranslateY(50);

    buttonGrid.setTranslateX(600);
    buttonGrid.setTranslateY(125);

    root.getChildren().addAll(topLeftGrid, buttonGrid, cardBackground, cardGrid);

    return root;
  }


  public void drawCard() {
    hand.clearHand();
    if (deck.getDeck().size() < 5) {
      deck.shuffleDeck();
    }
    hand.addCards(deck.dealHand(5));
    for (int i = 0; i < hand.getNumberOfCards(); i++) {
      cardImages.get(i).setImage(new Image("file:src/main/resources/cards/" + hand.getCard(i).getAsString() + ".png"));
    }
  }

  public void checkHand() {
    sumValue.setText(Integer.toString(hand.getSumOfCards()));
    heartsValue.setText(hand.cardsWithSuit('H'));
    flushValue.setText(Boolean.toString(hand.checkForFlush()));
    queenValue.setText(Boolean.toString(hand.cardInHand(new PlayingCard('S', 12))));

  }
}
