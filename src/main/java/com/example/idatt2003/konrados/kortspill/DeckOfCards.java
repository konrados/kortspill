package com.example.idatt2003.konrados.kortspill;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * <h1>DeckOfCards</h1>
 * The DeckOfCards class represents a deck of cards containing 52 cards.
 */
public class DeckOfCards {

  private final ArrayList<PlayingCard> deck;
  private final char[] suits = {'S', 'H', 'D', 'C'};

  private final Random random = new Random();


  /**
   * Checks if the number of cards to return is within bounds.
   *
   * @param n the number of cards to return
   * @throws IllegalArgumentException if n is less than 1 or greater than the size of the deck.
   */
  private void numberWitinBounds(int n) throws IllegalArgumentException {
    if (n < 1 || n > deck.size()) {
      throw new IllegalArgumentException(
          "Number of cards to return must be between 1 and " + deck.size());
    }
  }

  /**
   * Creates a new deck of cards. The deck contains 52 cards, where each card is a combination of a
   * suit and a face value.
   */
  public DeckOfCards() {
    deck = new ArrayList<>();
    for (char suit : suits) {
      for (int face = 1; face <= 13; face++) {
        deck.add(new PlayingCard(suit, face));
      }
    }
  }

  /**
   * Returns n random cards from the deck. throws an IllegalArgumentException if n is less than 1 or
   * greater than the size of the deck.
   *
   * @param n the number of cards to return
   * @return n random cards from the deck
   */

  public Collection<PlayingCard> dealHand(int n) {
    numberWitinBounds(n);
    Collection<PlayingCard> hand = new ArrayList<>();
    IntStream.range(0, n)
        .mapToObj(i -> deck.remove(random.nextInt(deck.size())))
        .forEach(hand::add);
    return hand;

  }

  /**
   * Returns the deck of cards.
   *
   * @return the deck of cards
   */
  public Collection<PlayingCard> getDeck() {
    return deck;
  }

  /**
   * Shuffles the deck of cards. The deck is cleared and refilled with 52 cards. Used to reset the
   * deck after dealing all the cards.
   */
  public void shuffleDeck() {
    deck.clear();
    for (char suit : suits) {
      for (int face = 1; face <= 13; face++) {
        deck.add(new PlayingCard(suit, face));
      }
    }
  }


}
