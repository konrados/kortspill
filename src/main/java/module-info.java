module com.example.kortspill {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.idatt2003.konrados.kortspill to javafx.fxml;
    exports com.example.idatt2003.konrados.kortspill;
}