package com.example.idatt2003.konrados.kortspill;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class HandOfCardsTest {

  @Nested
  @DisplayName("Positive tests")
  class PositiveTests {

    @Test
    @DisplayName("Constructor throws no exceptions")
    void testConstructorThrowsNoExceptions() {
      try {
        HandOfCards handOfCards = new HandOfCards();
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Constructor creates a hand of cards")
    void testConstructorCreatesHandOfCards() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        assertNotNull(handOfCards);
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Constructor creates a hand of cards with 0 cards")
    void testConstructorCreatesHandOfCardsWith0Cards() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        assertEquals(0, handOfCards.getNumberOfCards());
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("getNumberOfCards method returns the number of cards in the hand")
    void testGetNumberOfCardsMethodReturnsNumberOfCardsInHand() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        assertEquals(0, handOfCards.getNumberOfCards());
        handOfCards.addCard(new PlayingCard('H', 1));
        assertEquals(1, handOfCards.getNumberOfCards());
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("addCard method adds a card to the hand")
    void testAddCardMethodAddsCardToHand() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(new PlayingCard('H', 1));
        assertEquals(1, handOfCards.getNumberOfCards());
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());

      }
    }

    @Test
    @DisplayName("addCard method adds a card to the hand with the correct face")
    void testAddCardMethodAddsCardToHandWithCorrectValue() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(new PlayingCard('H', 1));
        assertEquals(1, handOfCards.getCard(0).getFace());
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("addCard method adds a card to the hand with the correct suit")
    void testAddCardMethodAddsCardToHandWithCorrectSuit() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(new PlayingCard('H', 1));
        assertEquals('H', handOfCards.getCard(0).getSuit());
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("addCards method adds a list of cards to the hand")
    void testAddCardsMethodAddsListOfCardsToHand() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        ArrayList<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('H', 1));
        cards.add(new PlayingCard('H', 2));
        handOfCards.addCards(cards);

        assertEquals(2, handOfCards.getNumberOfCards());
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("removeCard method removes a card from the hand")
    void testRemoveCardMethodRemovesCardFromHand() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        PlayingCard card = new PlayingCard('H', 1);
        handOfCards.addCard(card);
        handOfCards.removeCard(card);
        assertEquals(0, handOfCards.getNumberOfCards());
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("removeCard method removes the card at the given index from the hand")
    void testRemoveCardMethodRemovesCardAtGivenIndexFromHand() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        PlayingCard card = new PlayingCard('H', 1);
        handOfCards.addCard(card);
        handOfCards.removeCard(0);
        assertEquals(0, handOfCards.getNumberOfCards());
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("clearHand method removes all cards from the hand")
    void testClearHandMethodRemovesAllCardsFromHand() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(new PlayingCard('H', 1));
        handOfCards.addCard(new PlayingCard('H', 2));
        handOfCards.clearHand();
        assertEquals(0, handOfCards.getNumberOfCards());
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("getCard method returns the card at the given index")
    void testGetCardMethodReturnsCardAtGivenIndex() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        PlayingCard card = new PlayingCard('H', 1);
        handOfCards.addCard(card);
        assertEquals(card, handOfCards.getCard(0));
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("cardInHand returns true if card is in hand")
    void testCardInHandReturnsTrueIfCardIsInHand() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        PlayingCard card = new PlayingCard('H', 1);
        handOfCards.addCard(card);
        assertTrue(handOfCards.cardInHand(card));
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("cardInHand returns false if card is not in hand")
    void testCardInHandReturnsFalseIfCardIsNotInHand() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        PlayingCard card = new PlayingCard('H', 1);
        assertFalse(handOfCards.cardInHand(card));
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("checkForFlush method returns true when the hand is a flush")
    void testCheckForFlushMethodReturnsTrueWhenHandIsFlush() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(new PlayingCard('H', 1));
        handOfCards.addCard(new PlayingCard('H', 2));
        handOfCards.addCard(new PlayingCard('H', 3));
        handOfCards.addCard(new PlayingCard('H', 4));
        handOfCards.addCard(new PlayingCard('H', 5));
        assertTrue(handOfCards.checkForFlush());
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("checkForFlush method returns false when the hand is not a flush")
    void testCheckForFlushMethodReturnsFalseWhenHandIsNotFlush() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(new PlayingCard('H', 1));
        handOfCards.addCard(new PlayingCard('H', 2));
        handOfCards.addCard(new PlayingCard('H', 3));
        handOfCards.addCard(new PlayingCard('H', 4));
        handOfCards.addCard(new PlayingCard('S', 5));
        assertFalse(handOfCards.checkForFlush());
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("getSumOfCards method returns the sum of the cards in the hand")
    void testGetSumOfCardsMethodReturnsSumOfCardsInHand() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(new PlayingCard('H', 1));
        handOfCards.addCard(new PlayingCard('H', 2));
        handOfCards.addCard(new PlayingCard('H', 3));
        handOfCards.addCard(new PlayingCard('H', 4));
        handOfCards.addCard(new PlayingCard('H', 5));
        assertEquals(15, handOfCards.getSumOfCards());
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("cardsWithSuit returns a string with all cards with the given suit")
    void testCardWithSuitReturnsAllCardsWithGivenSuit() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(new PlayingCard('H', 1));
        handOfCards.addCard(new PlayingCard('H', 2));
        handOfCards.addCard(new PlayingCard('S', 3));
        handOfCards.addCard(new PlayingCard('H', 4));
        assertEquals("H1 H2 H4", handOfCards.cardsWithSuit('H'));
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }


  }

  @Nested
  @DisplayName("Negative tests")
  class NegativeTests {


    @Test
    @DisplayName("removeCard method throws an exception when hand is empty")
    void testRemoveCardMethodThrowsOnEmptyHand() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        handOfCards.removeCard(0);
        fail("An exception was not thrown");
      } catch (Exception e) {
        assertEquals("Hand is empty. No cards avalible", e.getMessage());
      }
    }

    @Test
    @DisplayName("removeCard method throws an exception when the index is out of bounds")
    void testRemoveCardMethodThrowsOnIndexOutOfBounds() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(new PlayingCard('H', 1));
        handOfCards.removeCard(-1);
        fail("An exception was not thrown");
      } catch (Exception e) {
        assertEquals("Number of cards to return must be between 0 and 0", e.getMessage());
      }
    }

    @Test
    @DisplayName("getCard method throws an exception when the index is out of bounds")
    void testGetCardMethodThrowsOnIndexOutOfBounds() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(new PlayingCard('H', 1));
        handOfCards.getCard(-1);
        fail("An exception was not thrown");
      } catch (Exception e) {
        assertEquals("Number of cards to return must be between 0 and 0", e.getMessage());
      }
    }

    @Test
    @DisplayName("getCard method throws an exception when hand is empty")
    void testGetCardMethodThrowsOnEmptyHand() {
      try {
        HandOfCards handOfCards = new HandOfCards();
        handOfCards.getCard(0);
        fail("An exception was not thrown");
      } catch (Exception e) {
        assertEquals("Hand is empty. No cards avalible", e.getMessage());
      }
    }
  }
}