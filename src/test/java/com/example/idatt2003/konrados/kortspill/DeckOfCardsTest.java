package com.example.idatt2003.konrados.kortspill;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class DeckOfCardsTest {

  @Nested
  @DisplayName("Positive tests")
  class PositiveTests {

    @Test
    @DisplayName("Constructor throws no exceptions with valid input")
    void testConstructorThrowsNoExceptions() {
      try {
        DeckOfCards deckOfCards = new DeckOfCards();
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Constructor creates a deck of cards")
    void testConstructorCreatesDeckOfCards() {
      try {
        DeckOfCards deckOfCards = new DeckOfCards();
        assertNotNull(deckOfCards);
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Constructor creates a deck of cards with 52 cards")
    void testConstructorCreatesDeckOfCardsWith52Cards() {
      try {
        DeckOfCards deckOfCards = new DeckOfCards();
        assertEquals(52, deckOfCards.getDeck().size());
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Constructor creates a deck of cards with 52 unique cards")
    void testConstructorCreatesDeckOfCardsWith52UniqueCards() {
      try {
        DeckOfCards deckOfCards = new DeckOfCards();
        assertEquals(52, deckOfCards.getDeck().stream().distinct().count());
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("dealHand method does not return null")
    void testDealHandMethodReturnsValidHand() {
      try {
        DeckOfCards deckOfCards = new DeckOfCards();
        Collection<PlayingCard> hand = deckOfCards.dealHand(5);
        assertNotNull(hand);
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("dealHand method returns a hand with the correct number of cards")
    void testDealHandMethodReturnsHandWithCorrectNumberOfCards() {
      try {
        DeckOfCards deckOfCards = new DeckOfCards();
        Collection<PlayingCard> hand = deckOfCards.dealHand(5);
        assertEquals(5, hand.size());
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("dealHand method returns a hand with unique cards")
    void testDealHandMethodReturnsHandWithUniqueCards() {
      try {
        DeckOfCards deckOfCards = new DeckOfCards();
        Collection<PlayingCard> hand = deckOfCards.dealHand(5);
        assertEquals(5, hand.stream().distinct().count());
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("dealHand method removes the cards from the deck")
    void testDealHandMethodRemovesCardsFromDeck() {
      try {
        DeckOfCards deckOfCards = new DeckOfCards();
        Collection<PlayingCard> hand = deckOfCards.dealHand(5);
        assertEquals(47, deckOfCards.getDeck().size());
        assertFalse(deckOfCards.getDeck().containsAll(hand));
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("getDeck method returns a deck with 52 cards")
    void testGetDeckMethodReturnsDeckWith52Cards() {
      try {
        DeckOfCards deckOfCards = new DeckOfCards();
        assertEquals(52, deckOfCards.getDeck().size());
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("shuffle method shuffles the deck")
    void testShuffleMethodShufflesCorrectly() {
      try {
        DeckOfCards deckOfCards = new DeckOfCards();
        deckOfCards.dealHand(50);
        deckOfCards.shuffleDeck();
        assertEquals(52, deckOfCards.getDeck().size());
      } catch (Exception e) {
        fail("An exception was thrown with the message: " + e.getMessage());
      }
    }


  }

  @Nested
  @DisplayName("Negative tests")
  class NegativeTests {

    @Test
    @DisplayName("dealHand method throws an exception when the number of cards is greater than the number of cards in the deck")
    void testDealHandMethodThrowsOnTooManyCards() {
      try {
        DeckOfCards deckOfCards = new DeckOfCards();
        deckOfCards.dealHand(53);
        fail("An exception was not thrown");
      } catch (Exception e) {
        assertEquals("Number of cards to return must be between 1 and 52", e.getMessage());
      }
    }

    @Test
    @DisplayName("dealHand method throws an exception when the number of cards is less than 1")
    void testDealHandMethodThrowsOnTooFewCards() {
      try {
        DeckOfCards deckOfCards = new DeckOfCards();
        deckOfCards.dealHand(0);
        fail("An exception was not thrown");
      } catch (Exception e) {
        assertEquals(e.getMessage(), "Number of cards to return must be between 1 and 52");
      }
    }

    @Test
    @DisplayName("dealHand method changes error based on number of cards in deck")
    void testDealHandMethodThrowsDifferentErrorBasedOnRemaningCards() {
      try {
        DeckOfCards deckOfCards = new DeckOfCards();
        deckOfCards.dealHand(50);
        deckOfCards.dealHand(3);
        fail("An exception was not thrown");
      } catch (Exception e) {
        assertEquals("Number of cards to return must be between 1 and 2", e.getMessage());
      }
    }
  }
}